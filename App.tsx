import React from 'react';

import { AuthProvider } from './app/authprovider';
import { CartProvider } from './app/cartprovider';
import { RootNavigator } from './app/navigation';

if (__DEV__) {
  import('./app/config/ReactotronConfig').then(() => console.log('Reactotron Configured'))
}

function App() {
  return (
    <AuthProvider>
      <CartProvider>
        <RootNavigator />
      </CartProvider>
    </AuthProvider>
  );
};

export default App;
