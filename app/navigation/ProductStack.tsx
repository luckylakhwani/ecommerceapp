import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { ProductListContainer, ProductDetailContainer, CheckoutContainer } from '../screens';
import { NavigationParamList } from '../navigation';

const Stack = createNativeStackNavigator<NavigationParamList>();

function ProductStack() {
    function addScreen(name: keyof NavigationParamList, component: any, title: string, showBackHeader: boolean) {
        return (<Stack.Screen
            name={name}
            component={component}
            options={{
                title: title,
                headerBackButtonMenuEnabled: showBackHeader
            }}
        />)
    }

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false
            }}>
            {addScreen("ProductList", ProductListContainer, "", false)}
            {addScreen("ProductDetails", ProductDetailContainer, "", false)}
            {addScreen("Checkout", CheckoutContainer, "", true)}
        </Stack.Navigator>
    )
}

export default ProductStack;