import React, { useContext } from 'react';

import { NavigationParamList } from '../navigation';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { NavigationContainer } from '@react-navigation/native';

import { ProductStack, UserAuthenticationStack } from '../navigation';
import { AuthContext } from '../authprovider';

const Stack = createNativeStackNavigator<NavigationParamList>();

function RootNavigator() {
    const { user } = useContext(AuthContext);

    function unAuthenticatedStack() {
        return (
            <UserAuthenticationStack />
        );
    }

    function authenticatedStack() {
        return (<ProductStack />)
    }

    return (
        <NavigationContainer>
            {user ? authenticatedStack() : unAuthenticatedStack()}
        </NavigationContainer>
    )
}

export default RootNavigator;