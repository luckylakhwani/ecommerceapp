import { RouteProp } from '@react-navigation/native';
import { NativeStackNavigationProp } from '@react-navigation/native-stack';

import { Product } from '../screens/products/types';

export type NavigationParamList = {
    Login: undefined,
    Register: undefined,
    Checkout: undefined,
    ProductList: undefined,
    ProductDetails: {
        product: Product
    }
};

export type NavigationProps<T extends keyof NavigationParamList> = {
    navigation: NativeStackNavigationProp<NavigationParamList, T>;
    route: RouteProp<NavigationParamList, T>;
};