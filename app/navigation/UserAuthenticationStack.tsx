import React, { useEffect, useState } from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

import { LoginContainer, RegisterContainer } from '../screens';
import { NavigationParamList } from '../navigation';
import { LocalStorage, Logger, UserDefaultKeys } from '../utils';

const Stack = createNativeStackNavigator<NavigationParamList>();

function UserAuthenticationStack() {

    const [isUserRegistered, setIsUserRegistered] = useState(false);

    useEffect(() => {
        checkIsUserRegistered();
    }, []);

    function checkIsUserRegistered() {
        LocalStorage.getData(UserDefaultKeys.isRegistered).then((data) => {
            setIsUserRegistered(data === 'true');
            Logger.log('isUserRegistered', isUserRegistered);
        }).catch((err) => { })
    }

    function addScreen(name: keyof NavigationParamList, component: any, title: string, showBackHeader: boolean) {
        return (<Stack.Screen
            name={name}
            component={component}
            options={{
                title: title,
                headerBackButtonMenuEnabled: showBackHeader
            }}
        />)
    }

    return (
        <Stack.Navigator
            screenOptions={{
                headerShown: false,
                animation: 'none'
            }}
            initialRouteName={'Register'}>
            {addScreen("Login", LoginContainer, "", false)}
            {!isUserRegistered ? addScreen("Register", RegisterContainer, "", false) : null}
        </Stack.Navigator>
    )
}

export default UserAuthenticationStack;