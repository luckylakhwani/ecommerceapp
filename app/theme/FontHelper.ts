export const Fonts = {
    Regular: 'CircularStd-Book',
    Bold: 'CircularStd-Bold'
}

export const FontSize = {
    Small: 15,
    Medium: 17,
    Large: 22,
    XXLarge: 30
}