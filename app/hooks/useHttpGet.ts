import { HttpClient, HttpClientRequestParameters } from "../utils";

export function getData<T>(apiUrl: string) {
    const httpClient = new HttpClient();
    const getParameters: HttpClientRequestParameters = {
        apiUrl: apiUrl
    };

    return httpClient.get<T>(getParameters);
}
