import { UserInfo } from "../authprovider/types";

export default function getUserInfo() {
    const userInfo: UserInfo = {
        fullName: "Lucky Lakhwani",
        email: "lucky@gmail.com"
    };

    return userInfo;
}
