import React, { useState } from 'react';
import { View } from 'react-native';

import { Button, TextField, BoldText, KeyboardAvoidContainer } from '../../common-components';

import { Logger } from '../../utils';

import { styles } from '../register/styles';

interface LoginProps {
    onLogin: (email: string, password: string) => void;
}

function Login(props: LoginProps) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    return (
        <KeyboardAvoidContainer>
            <View style={styles.scrollContainer}>
                <BoldText
                    text={"Login"}
                    style={styles.titleStyle}
                />
                <TextField
                    style={styles.textFieldStyle}
                    placeholder="Email"
                    onChangeText={(value: string) => {
                        setEmail(value);
                    }}
                    keyboardType='email-address'
                />
                <TextField
                    style={styles.textFieldStyle}
                    placeholder="Password"
                    onChangeText={(value: string) => {
                        setPassword(value);
                    }}
                    secureTextEntry={true}
                    keyboardType="default"
                />
                <Button
                    title="Login"
                    onPress={() => { props.onLogin(email, password) }}
                />
            </View>
        </KeyboardAvoidContainer>
    )
}

export default Login;