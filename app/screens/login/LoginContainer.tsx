import React, { useContext } from 'react';

import { SafeAreaContainer } from '../../common-components';
import Login from './Login';

import { NavigationProps } from '../../navigation';

import { AuthContext } from '../../authprovider';
import { showAlert, validateEmail } from '../../utils';

function LoginContainer({ navigation }: NavigationProps<'Login'>) {
    const { login } = useContext(AuthContext);

    const onLogin = async (email: string, password: string) => {
        if (email === '' || !validateEmail(email)) {
            showAlert("Error", "Please enter a valid email");
            return;
        }

        if (password === '') {
            showAlert("Error", "Please enter a valid password");
            return;
        }

        await login(email, password);
    }

    return (
        <SafeAreaContainer>
            <Login onLogin={onLogin} />
        </SafeAreaContainer>
    )
}

export default LoginContainer;