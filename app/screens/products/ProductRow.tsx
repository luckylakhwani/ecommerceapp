import React, { useState } from 'react';
import { Pressable, View, StyleSheet } from 'react-native';

import { Product } from './types';

import { colors, fonts } from '../../theme';

import { RegularText, BoldText, Ratings, QuantitySelector } from '../../common-components';

import FastImage from 'react-native-fast-image'
import { formatPrice } from '../../utils';

interface ProductRowProps {
    product: Product;
    onPress: (product: Product) => void;
    onAdd: (product: Product) => void;
    onRemove: (id: number) => void;
}

function ProductRow(props: ProductRowProps) {
    const { product } = props;

    const [quantity, setQuantity] = useState(product?.quantity || 0);

    return (
        <Pressable onPress={() => { props.onPress(product) }} style={styles.container}>
            <FastImage style={styles.image}
                resizeMode={FastImage.resizeMode.contain}
                source={{ uri: product.image, priority: FastImage.priority.normal }} />
            <View style={styles.rightContainer}>
                <RegularText style={styles.title} text={product.title} />
                <Ratings
                    ratings={product.rating}
                />
                <BoldText
                    text={formatPrice(product.price)}
                    style={styles.price} />
                <QuantitySelector
                    id={product.id}
                    setQuantity={setQuantity}
                    onAdd={() => {
                        props.onAdd(product)
                    }}
                    onRemove={() => props.onRemove(product.id)}
                />
            </View>

        </Pressable>
    )
}
export default ProductRow;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        borderWidth: 1,
        borderColor: colors.gray,
        borderRadius: 10,
        backgroundColor: colors.white,
        marginVertical: 5,
        margin: 10
    },
    image: {
        flex: 2,
        height: 150,
        resizeMode: 'contain',
    },
    rightContainer: {
        padding: 10,
        flex: 3,
    },
    title: {
        fontSize: fonts.fontSize.Medium,
        textAlign: 'left'
    },
    price: {
        fontSize: fonts.fontSize.Medium,
        textAlign: 'left',
        marginBottom: 10
    },
    ratingsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 5,
    },
    star: {
        margin: 2,
    },
});