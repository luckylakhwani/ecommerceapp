import React, { useContext } from "react";
import { StyleSheet } from "react-native";

import { Button } from "../../common-components";

import { useNavigation } from "@react-navigation/native";

import { CartContext } from "../../cartprovider";

function CheckoutButton() {
    const navigation = useNavigation();
    const { totalProducts } = useContext(CartContext);

    return (<Button
        disabled={totalProducts === 0}
        containerStyle={[styles.checkout, { opacity: totalProducts === 0 ? 0.5 : 1 }]}
        title="Checkout"
        onPress={() => navigation.navigate('Checkout')}
    />)

}

const styles = StyleSheet.create({
    checkout: {
        width: '100%',
        borderRadius: 0,
        height: 60
    }
})

export default CheckoutButton;