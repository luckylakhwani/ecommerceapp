import { useReducer } from 'react';
import { Endpoints, getAPIURL } from '../../config';

import { getData } from '../../hooks';
import { Logger, replaceParamsInStr } from '../../utils';
import { Product } from './types';

const actions = {
    GET_PRODUCTS: "GET_PRODUCTS",
    IS_LOADING: "IS_LOADING",
    IS_ERROR: "IS_ERROR",
}

function reducer(state: any, action: any) {
    if (action) {
        switch (action.type) {
            case actions.GET_PRODUCTS:
                return {
                    ...state,
                    products: action.payload
                }

            case actions.IS_LOADING:
                return {
                    ...state,
                    isLoading: action.payload
                }

            case actions.IS_ERROR:
                return {
                    ...state,
                    error: action.payload
                }
        }
    }
}

export function useProductsData() {
    let [data, dispatch] = useReducer(reducer, {
        error: null,
        isLoading: false,
        products: null,
    });

    async function getProductList() {
        const endPoint = replaceParamsInStr(Endpoints.getProducts, { limit: 15 });
        Logger.log("endPoint", endPoint);
        try {
            dispatch({ type: actions.IS_LOADING, payload: true })

            const res = await getData<[Product]>(getAPIURL(endPoint));
            Logger.log('Products', res);
            dispatch({ type: actions.GET_PRODUCTS, payload: res })
        }
        catch (err) {
            dispatch({ type: actions.IS_ERROR, payload: err });

        }
        finally {
            dispatch({ type: actions.IS_LOADING, payload: false });
        }
    }

    return {
        ...data,
        getProductList: () => getProductList()
    }
}