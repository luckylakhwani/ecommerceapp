import React, { useContext, useState } from 'react';
import { StyleSheet, ScrollView, View } from 'react-native';

import { BoldText, RegularText, QuantitySelector, SafeAreaContainer, Ratings, CustomHeader } from '../../common-components';
import CheckoutButton from './CheckoutButton';

import { NavigationProps } from '../../navigation';

import { colors, fonts } from '../../theme';

import FastImage from 'react-native-fast-image'

import { CartContext } from '../../cartprovider';
import { formatPrice } from '../../utils';


function ProductDetailContainer({ route }: NavigationProps<'ProductDetails'>) {
    const product = route.params.product;
    const { addProduct, removeProduct } = useContext(CartContext);
    const [quantity, setQuantity] = useState(product.quantity || 0);

    return (
        <SafeAreaContainer>
            <CustomHeader showBackButton={true} />
            <ScrollView style={{
                width: '100%', backgroundColor: colors.white
            }}>
                <View style={styles.scrollContainer}>
                    {/* <View style={styles.rowContainer}> */}
                    <BoldText
                        style={styles.title}
                        text={product.title} />

                    {/* </View> */}
                    <FastImage style={styles.image}
                        resizeMode={FastImage.resizeMode.contain}
                        source={{ uri: product.image, priority: FastImage.priority.normal }} />
                    <View style={styles.rowContainer}>
                        <View>
                            <BoldText
                                style={styles.price}
                                text={formatPrice(product.price)} />
                            <Ratings
                                ratings={product.rating}
                            />
                        </View>
                        <View style={styles.quntityContainer}>
                            <QuantitySelector
                                id={product.id}
                                setQuantity={setQuantity}
                                onAdd={() => { addProduct(product) }}
                                onRemove={() => { removeProduct(product.id) }}
                            />
                        </View>
                    </View>
                    <RegularText
                        style={styles.description}
                        text={product.description} />
                </View>
            </ScrollView>
            <CheckoutButton />
        </SafeAreaContainer>
    )
}

export default ProductDetailContainer;

const styles = StyleSheet.create({
    image: {
        width: '100%',
        height: 350,
        marginBottom: 20
    },
    scrollContainer: {
        flex: 1,
        alignItems: 'center',
        marginBottom: 40,
        backgroundColor: colors.white
    },
    title: {
        fontSize: fonts.fontSize.Large,
        margin: 15,
        flex: 1,
    },
    description: {
        fontSize: fonts.fontSize.Medium,
        textAlign: 'left',
        padding: 10,
        marginTop: 20,
    },
    rowContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-around'
    },
    ratingsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    star: {
        margin: 2,
    },
    price: {
        fontSize: fonts.fontSize.Large,
        textAlign: 'left',
        marginTop: 10
    },
    quntityContainer: { justifyContent: 'center' }
})