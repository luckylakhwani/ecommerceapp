import React from 'react';
import { FlatList, ListRenderItem } from 'react-native';

import ProductRow from './ProductRow';

import { Product } from './types';

interface ProductListProps {
    products: Product[];
    onPress: (product: Product) => void;
    onAdd: (product: Product) => void;
    onRemove: (id: number) => void;
}

function ProductList({ products, onPress, onAdd, onRemove }: ProductListProps) {

    const renderItem: ListRenderItem<Product> = ({ item }) => (< ProductRow
        product={item}
        onPress={onPress}
        onAdd={onAdd}
        onRemove={onRemove}
    />);

    return (
        <FlatList<Product>
            data={products}
            renderItem={renderItem}
            keyExtractor={item => item.title}
            initialNumToRender={5}
            removeClippedSubviews={true}
        />
    )
}

export default ProductList;