import React, { useContext, useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';

import { CartContext } from '../../cartprovider';

import { SafeAreaContainer, CustomHeader } from '../../common-components';
import CheckoutButton from './CheckoutButton';
import ProductList from './ProductList';

import { NavigationProps } from '../../navigation';

import { colors } from '../../theme';

import { useProductsData } from './useProductsData';

function ProductsListContainer({ navigation }: NavigationProps<'ProductList'>) {
    const { products, isLoading, getProductList } = useProductsData();
    const { addProduct, removeProduct } = useContext(CartContext);

    useEffect(() => {
        getProductList();
    }, [])

    return (
        <SafeAreaContainer>
            <View style={{ flex: 1 }}>
                <CustomHeader showBackButton={false} />
                <ProductList
                    products={products}
                    onPress={(product) => {
                        navigation.navigate('ProductDetails', { product })
                    }}
                    onAdd={addProduct}
                    onRemove={removeProduct}
                />
            </View>
            <ActivityIndicator
                style={{ width: 30, height: 30, alignSelf: 'center', position: 'absolute' }}
                color={colors.primaryDark}
                animating={isLoading}
                size='large'
            />
            <CheckoutButton />
        </SafeAreaContainer>
    )
}

export default ProductsListContainer;