import { StyleSheet } from 'react-native';
import { fonts } from '../../theme';

export const styles = StyleSheet.create({
    titleStyle: {
        fontSize: fonts.fontSize.XXLarge,
        marginBottom: 40,
        marginTop: 150
    },
    textFieldStyle: {
        marginBottom: 20
    },
    scrollContainer: {
        width: '100%',
        alignItems: 'center',
        justifyContent: 'flex-start'
    }
})