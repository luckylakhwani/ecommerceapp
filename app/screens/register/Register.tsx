import React, { useState } from 'react';
import { View } from 'react-native';

import { Button, TextField, BoldText, KeyboardAvoidContainer } from '../../common-components';

import { styles } from './styles';

interface RegisterProps {
    onRegister: (email: string, password: string, fullName: string) => void;
}

function Register(props: RegisterProps) {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [fullName, setFullName] = useState('');

    return (
        <KeyboardAvoidContainer>
            <View style={styles.scrollContainer}>
                <BoldText
                    text={"Registration"}
                    style={styles.titleStyle}
                />
                <TextField
                    style={styles.textFieldStyle}
                    placeholder="Full Name"
                    onChangeText={(value: string) => {
                        setFullName(value);
                    }}
                    keyboardType="default"
                />
                <TextField
                    style={styles.textFieldStyle}
                    placeholder="Email"
                    onChangeText={(value: string) => {
                        setEmail(value);
                    }}
                    keyboardType='email-address'
                />
                <TextField
                    style={styles.textFieldStyle}
                    placeholder="Password"
                    onChangeText={(value: string) => {
                        setPassword(value);
                    }}
                    secureTextEntry={true}
                    keyboardType="default"
                />

                <Button
                    title="Register"
                    onPress={() => props.onRegister(email, password, fullName)}
                />
            </View>
        </KeyboardAvoidContainer>
    )
}

export default Register;