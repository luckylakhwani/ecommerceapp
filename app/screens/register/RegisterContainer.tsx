import React from 'react';
import { SafeAreaContainer } from '../../common-components';

import { NavigationProps } from '../../navigation';

import { LocalStorage, UserDefaultKeys, showAlert, validateEmail } from '../../utils';

import Register from './Register';

function RegisterContainer({ navigation }: NavigationProps<'Register'>) {

    function onRegister(email: string, password: string, fullName: string) {
        if (fullName === '') {
            showAlert("Error", "Please enter a valid Name");
            return;
        }

        if (email === '' || !validateEmail(email)) {
            showAlert("Error", "Please enter a valid email");
            return;
        }

        if (password === '') {
            showAlert("Error", "Please enter a valid password");
            return;
        }

        setUserRegistered(fullName);
        navigation.navigate('Login');
    }

    async function setUserRegistered(name: string) {
        await LocalStorage.setData(UserDefaultKeys.isRegistered, 'true');
        await LocalStorage.setData(UserDefaultKeys.userName, name);
    }

    return (
        <SafeAreaContainer>
            <Register onRegister={onRegister} />
        </SafeAreaContainer>
    )
}

export default RegisterContainer;