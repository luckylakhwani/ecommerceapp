import React, { useContext } from 'react';
import { StyleSheet, View } from 'react-native';

import { CartContext } from '../../cartprovider';

import { Button, SafeAreaContainer } from '../../common-components';
import Checkout from './Checkout';

import { NavigationProps } from '../../navigation';

import { showAlert } from '../../utils';

function CheckoutContainer({ navigation }: NavigationProps<'Checkout'>) {
    const { cartProducts, totalAmount, clearCart } = useContext(CartContext);

    return (
        <SafeAreaContainer>
            <View style={{ flex: 1, width: '100%' }}>
                <Checkout products={cartProducts} totalAmount={totalAmount} />
            </View>
            <Button
                containerStyle={styles.pay}
                title="Pay"
                onPress={() => {
                    showAlert("Order Successful", "Your order is placed", "OK", () => {
                        clearCart();
                        navigation.goBack()
                    })
                }}
            />
        </SafeAreaContainer>
    )
}

export default CheckoutContainer;

const styles = StyleSheet.create({
    pay: {
        width: '100%',
        borderRadius: 0,
        height: 60
    }
})
