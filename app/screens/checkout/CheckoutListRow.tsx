import React from 'react';
import { StyleSheet, View } from 'react-native';

import { BoldText, RegularText } from '../../common-components';

import { fonts } from '../../theme';

interface CheckoutListRowProps {
    title: string;
    value: string;
}

function CheckoutListRow({ title, value }: CheckoutListRowProps) {
    return (
        <View style={styles.container}>
            <RegularText style={styles.titleText} text={title} />
            <BoldText style={styles.valueText} text={value} />
        </View>
    )
}

export default CheckoutListRow;

const styles = StyleSheet.create({
    container: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 5
    },
    titleText: {
        fontSize: fonts.fontSize.Small,
        flex: 2,
        textAlign: 'left',
        paddingLeft: 10
    },
    valueText: {
        fontSize: fonts.fontSize.Medium,
        flex: 1,
        textAlign: 'right',
        paddingRight: 10
    }
})
