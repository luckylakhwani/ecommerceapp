import React from 'react';
import { View, StyleSheet, ScrollView } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { images } from '../../assets/images';

import { CenteredView, BoldText, Separator, ImageButton } from '../../common-components';
import CheckoutListRow from './CheckoutListRow';

import { colors, fonts } from '../../theme';

import { Product } from '../products/types';

import { formatPrice } from '../../utils';

interface CheckoutProps {
    products: Product[];
    totalAmount: number;
}

function Checkout({ products, totalAmount }: CheckoutProps) {
    const navigation = useNavigation();

    function renderProducts() {
        return products.map((product, index) => {
            return (
                <View
                    key={product.title}
                >
                    <CheckoutListRow
                        title={product.title}
                        value={formatPrice(product.price) + " x " + product.quantity}
                    />
                    {index !== (products.length - 1) ? <Separator /> : null}
                </View>)
        })

    }

    function renderTotal() {
        return (
            <CheckoutListRow
                title={"Total Amount"}
                value={formatPrice(totalAmount)}
            />
        )
    }

    return (
        // <CenteredView>
        <View style={styles.mainContainer}>
            <View style={styles.headerContainer}>
                <ImageButton
                    imageStyle={{ width: 30, height: 30, marginBottom: 0, marginRight: 70 }}
                    imageSrc={images.back}
                    onPress={navigation.goBack}
                />
                <BoldText style={styles.title} text="Checkout" />
            </View>
            <ScrollView
                style={styles.scrollStyle}
            >
                <View style={styles.container}>
                    {renderProducts()}
                </View>
                <View style={styles.container}>
                    {renderTotal()}
                </View>
            </ScrollView>
        </View>
        // </CenteredView>
    )
}

export default Checkout;

const styles = StyleSheet.create({
    mainContainer: {
        width: '100%',
        height: '100%'
    },
    headerContainer: {
        flexDirection: 'row',
        backgroundColor: colors.primaryDark,
        height: 60,
        alignItems: 'center'
    },
    container: {
        width: '95%',
        padding: 10,
        borderColor: colors.gray,
        borderWidth: 1,
        borderRadius: 8,
        alignSelf: 'center',
        marginTop: 20
    },
    title: {
        fontSize: fonts.fontSize.XXLarge,
        color: colors.white,
        marginTop: 0,
        flex: 2,
        textAlign: 'left'
    },
    scrollStyle: {
        flex: 1,
        marginBottom: 20
    }
})