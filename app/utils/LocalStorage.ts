import RNUserDefaults from 'rn-user-defaults';
import { Logger } from './index';

export default class LocalStorage {
    static async setData(key: string, value: string) {
        try {
            await RNUserDefaults.set(key, value);
        } catch (error) {
            Logger.log('Error while storing data in Local storage', error);
        }
    }

    static async getData(key: string) {
        try {
            const data = await RNUserDefaults.get(key);
            if (data !== undefined) {
                return data;
            }
        } catch (error) {
            Logger.log('Error while getting data from Local storage', error);
        }
    }

    static async removeData(key: string) {
        try {
            await RNUserDefaults.clear(key);
        } catch (error) {
            Logger.log('Error while removing data from Local storage', error);
        }
    }

    static async clearStorage() {
        try {
            await RNUserDefaults.clearAll();
        } catch (error) {
            Logger.log('Error while clearing data from Local storage', error);
        }
    }
}


export const UserDefaultKeys = {
    isRegistered: "isRegistered",
    userName: "userName",
}