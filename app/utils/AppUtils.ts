import { Alert } from "react-native";

export const replaceParamsInStr = (str: string, obj: any) => {
    if (obj) {
        for (let [key, value] of Object.entries(obj)) {
            str = str.replace(`{${key}}`, value as any);
        }
    }
    return str;
};

export const formatPrice = (price: number) => {
    return "₹ " + price.toFixed(2).toString();
}

export const showAlert = (title: string, message: string, buttonTitle?: string, callback?: () => void) => {
    Alert.alert(title, message, [
        {
            text: buttonTitle || "OK",
            onPress: callback
        }
    ])
}

export const validateEmail = (email: string) => {
    return email
        .toLowerCase()
        .match(
            /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
        );
};