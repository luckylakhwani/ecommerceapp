import { createContext } from "react";
import { CartContextProps } from "./types";

const cartContextDefaultProps: CartContextProps = {
    cartProducts: [],
    addProduct: () => { },
    removeProduct: () => { },
    totalAmount: 0,
    totalProducts: 0,
    getProduct: () => undefined,
    clearCart: () => { }

}

export const CartContext = createContext<CartContextProps>(cartContextDefaultProps);

