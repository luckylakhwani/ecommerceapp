import React, { useState } from "react";
import { CartContext } from "./CartContext";
import { Product } from "../screens";

interface CartProviderProps {
    children?: React.ReactNode;
}

function CartProvider(props: CartProviderProps) {
    const [products, setProducts] = useState<Product[]>([]);

    function addProduct(product: Product) {
        setProducts((prevProducts) => {
            //Item already in cart
            const productInCart: Product | undefined = prevProducts.find(p => p.id === product?.id);
            if (productInCart) {
                return prevProducts.map(p => p.id === product.id ?
                    { ...p, quantity: p.quantity + 1 } : p);
            }

            //Item not already added
            return [...prevProducts, { ...product, quantity: 1 }];
        });
    }

    function removeProduct(id: number) {
        setProducts((prevProducts) =>
            prevProducts.reduce((result, p) => {
                if (p.id === id) {
                    if (p.quantity === 1) return result;
                    return [...result, { ...p, quantity: p.quantity - 1 }];
                } else {
                    return [...result, p];
                }
            }, [] as Product[])
        )
    }

    function getProductFromId(id: number) {
        const productInCart: Product | undefined = products.find(p => p.id === id);
        return productInCart;
    }

    function getTotalAmount() {
        return products.reduce((result: number, p: Product) => result + (p.price * p.quantity), 0);
    }

    function getTotalProducts() {
        return products.reduce((result: number, p: Product) => result + p.quantity, 0);
    }

    function clearCart() {
        setProducts([]);
    }

    return (
        <CartContext.Provider
            value={{
                cartProducts: products,
                addProduct: addProduct,
                removeProduct: removeProduct,
                totalAmount: getTotalAmount(),
                totalProducts: getTotalProducts(),
                getProduct: (id: number) => getProductFromId(id),
                clearCart: clearCart
            }}
        >
            {props.children}
        </CartContext.Provider >
    )
}

export default CartProvider;