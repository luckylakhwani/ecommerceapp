import { Product } from "../screens/products/types";

export interface CartContextProps {
    cartProducts: Product[];
    addProduct: (product: Product) => void;
    removeProduct: (id: number) => void;
    totalAmount: number;
    totalProducts: number;
    getProduct: (id: number) => Product | undefined;
    clearCart: () => void;
}