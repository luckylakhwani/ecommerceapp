export interface AuthContextProps {
    login: (emailId: string, password: string) => {};
    logout: () => void;
    user: UserInfo | null;
}

export interface UserInfo {
    fullName: string,
    email: string
}