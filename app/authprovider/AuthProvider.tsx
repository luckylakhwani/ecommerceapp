import React, { useState } from "react";
import getUserInfo from "../mocks/getUserInfo";
import { AuthContext } from "./AuthContext";
import { LocalStorage, Logger, UserDefaultKeys } from "../utils";
import { UserInfo } from "./types";

interface AuthProviderProps {
    children?: React.ReactNode;
}

function AuthProvider(props: AuthProviderProps) {
    const [userInfo, setUserInfo] = useState<UserInfo | null>(null);

    async function onLogin(email: string, password: string) {
        let res = await getUserInfo()
        const username = await LocalStorage.getData(UserDefaultKeys.userName);
        res.fullName = username;
        setUserInfo(res);
        return res;
    }

    function onLogout() {
        Logger.log("Logout")
        setUserInfo(null);
    }

    return (
        <AuthContext.Provider
            value={{
                login: (emailId, password) => {
                    return onLogin(emailId, password);
                },
                logout: onLogout,
                user: userInfo
            }}
        >
            {props.children}
        </AuthContext.Provider >
    )
}

export default AuthProvider;