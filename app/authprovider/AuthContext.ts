import { createContext } from "react";
import { AuthContextProps } from "./types";

const authContextDefaultProps: AuthContextProps = {
    login: async () => { },
    logout: () => { },
    user: null
}

export const AuthContext = createContext<AuthContextProps>(authContextDefaultProps)
