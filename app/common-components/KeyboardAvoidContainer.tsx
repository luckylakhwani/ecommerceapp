import React from 'react';
import { KeyboardAvoidingView, TouchableWithoutFeedback, Keyboard, Platform, ScrollView, StyleSheet } from 'react-native';

interface LoginProps {
    children?: React.ReactNode;
}

function KeyboardAvoidContainer(props: LoginProps) {
    return (
        <KeyboardAvoidingView
            behavior={Platform.OS === "ios" ? "padding" : "height"}
            style={styles.mainContinerStyle}>
            <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <ScrollView style={styles.scrollViewStyle}>
                    {props.children}
                </ScrollView>
            </TouchableWithoutFeedback>
        </KeyboardAvoidingView >
    )
}

export default KeyboardAvoidContainer;

export const styles = StyleSheet.create({
    mainContinerStyle: {
        width: '100%',
        height: '100%'
    },
    scrollViewStyle: {
        width: '100%'
    }
})