import React from "react";
import { Text, StyleSheet } from "react-native";

import { colors, fonts } from "../theme";

interface BoldTextProps {
    style?: {};
    text: string;
}

function BoldText(props: BoldTextProps) {
    return (
        <Text
            style={[styles.texStyle, props.style]}
        >
            {props.text}
        </Text>
    )
}

const styles = StyleSheet.create({
    texStyle: {
        color: colors.textColor,
        textAlign: "center",
        backgroundColor: colors.transparent,
        fontFamily: fonts.fontFamily.Bold,
        fontSize: fonts.fontSize.Small
    }
})

export default BoldText;