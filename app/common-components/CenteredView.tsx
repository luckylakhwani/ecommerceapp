import React from "react";
import { View, StyleSheet } from "react-native";

interface SafeAreaProps {
    style?: {};
    children?: React.ReactNode;
}

function CenteredView(props: SafeAreaProps) {
    return (
        <View style={[styles.containerStyle, props.style]}>
            {props.children}
        </View>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default CenteredView;