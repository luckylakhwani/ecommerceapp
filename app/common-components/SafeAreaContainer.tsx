import React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

interface SafeAreaProps {
    style?: {};
    children?: React.ReactNode;
}

function SafeAreaContainer(props: SafeAreaProps) {
    return (
        <SafeAreaView style={[styles.containerStyle, props.style]}>
            {props.children}
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})

export default SafeAreaContainer;