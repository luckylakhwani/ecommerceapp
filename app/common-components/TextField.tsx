import React from "react";
import { TextInput, StyleSheet, TextInputProps } from "react-native";

import { colors } from "../theme";

interface TextFieldProps extends TextInputProps {
}

function TextField(props: TextFieldProps) {
    return (
        <TextInput
            keyboardType={props.keyboardType}
            style={[styles.containerStyle, props.style]}
            placeholder={props.placeholder}
            value={props.value}
            onChangeText={props.onChangeText}
            secureTextEntry={props.secureTextEntry || false}
        >
        </TextInput>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '80%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.black,
        paddingHorizontal: 5
    }
})

export default TextField;