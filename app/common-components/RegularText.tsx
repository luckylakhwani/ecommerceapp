import React from "react";
import { Text, StyleSheet } from "react-native";

import { colors, fonts } from "../theme";

interface RegularTextProps {
    style?: {};
    text: string;
}

function RegularText(props: RegularTextProps) {
    return (
        <Text
            style={[styles.texStyle, props.style]}
        >
            {props.text}
        </Text>
    )
}

const styles = StyleSheet.create({
    texStyle: {
        color: colors.textColor,
        textAlign: "center",
        backgroundColor: colors.transparent,
        fontFamily: fonts.fontFamily.Regular,
        fontSize: fonts.fontSize.Small
    }
})

export default RegularText;