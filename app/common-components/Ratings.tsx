import React from 'react';
import { View, StyleSheet } from 'react-native';

import FontAwesome from 'react-native-vector-icons/FontAwesome';

import { colors } from '../theme';
import RegularText from './RegularText';

import { ProductRating } from '../screens/products/types';

interface RatingsProps {
    ratings: ProductRating;
}

function Ratings({ ratings }: RatingsProps) {
    return (
        <View style={styles.ratingsContainer}>
            {[0, 0, 0, 0, 0].map((element, i) => (
                <FontAwesome
                    key={`${i}`}
                    style={styles.star}
                    name={i < Math.floor(ratings.rate) ? 'star' : 'star-o'}
                    size={15}
                    color={colors.orange}
                />
            ))}
            <RegularText text={"(" + ratings.count.toString() + ")"} />
        </View>
    )
}

const styles = StyleSheet.create({
    ratingsContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        marginVertical: 10,
    },
    star: {
        margin: 2,
    }
})

export default Ratings;