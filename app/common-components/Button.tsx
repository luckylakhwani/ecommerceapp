import React from "react";
import { Pressable, StyleSheet, Text } from "react-native";

import { colors, fonts } from "../theme";

interface ButtonProps {
    containerStyle?: {};
    titleStyle?: {};
    onPress?: () => void;
    title: string;
    disabled?: boolean;
}

function Button(props: ButtonProps) {
    return (
        <Pressable
            onPress={props.onPress}
            style={[styles.containerStyle, props.containerStyle]}
            disabled={props.disabled}
        >
            <Text style={[styles.titleStyle, props.titleStyle]}>
                {props.title}
            </Text>
        </Pressable>
    )
}

const styles = StyleSheet.create({
    containerStyle: {
        width: '80%',
        height: 45,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        backgroundColor: colors.primaryDark
    },
    titleStyle: {
        color: colors.white,
        textAlign: 'center',
        fontFamily: fonts.fontFamily.Regular,
        fontSize: fonts.fontSize.Medium
    }
})

export default Button;