import React, { useContext } from "react";
import { View, StyleSheet } from 'react-native';

import { useNavigation } from '@react-navigation/native';

import { colors, fonts } from "../theme";
import { images } from "../assets/images";

import { RegularText, ImageButton, Button, BoldText } from "../common-components";

import { AuthContext } from "../authprovider";
import { CartContext } from "../cartprovider";

interface CustomHeaderProps {
    showBackButton: boolean;
}
function CustomHeader({ showBackButton }: CustomHeaderProps) {
    const { user, logout } = useContext(AuthContext);
    const { totalProducts } = useContext(CartContext);

    const navigation = useNavigation();

    function renderBackButton() {
        return (
            showBackButton ?
                <View style={{ width: 40, height: '100%', alignItems: 'center' }}>
                    <ImageButton
                        imageStyle={{ width: 30, height: 30 }}
                        imageSrc={images.back}
                        onPress={navigation.goBack}
                    />
                </View> : null
        )
    }

    return (
        <View style={styles.containerStyle}>
            {renderBackButton()}
            <View style={{ flex: 1 }}>
                <BoldText
                    style={styles.userNameTextStyle}
                    text={user?.fullName || "N.A"}
                />
                <Button
                    containerStyle={styles.buttonStyle}
                    title={"Logout"}
                    onPress={logout}
                />
            </View>
            <View style={styles.cartContainerStyle}>
                <RegularText
                    style={styles.cartCountTextStyle}
                    text={totalProducts.toString()} />
                <ImageButton
                    containerStyle={styles.cartImageConatinerStyle}
                    imageStyle={styles.cartImageStyle}
                    imageSrc={images.cart}
                />
            </View>

        </View >
    )
}
const styles = StyleSheet.create({
    containerStyle: {
        width: '100%',
        height: 80,
        backgroundColor: colors.primaryDark,
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        flexDirection: 'row'
    },
    userNameTextStyle: {
        fontSize: fonts.fontSize.Medium,
        color: colors.white,
        margin: 10,
        marginBottom: 0,
        textAlign: 'left'
    },
    buttonStyle: {
        width: 70,
        backgroundColor: colors.transparent
    },
    buttonTextStyle: {
        fontSize: fonts.fontSize.Small
    },
    cartContainerStyle: {
        height: '100%',
        justifyContent: 'center',
    },
    cartImageConatinerStyle: {
        marginRight: 20
    },
    cartImageStyle: {
        width: 25,
        height: 25
    },
    cartCountTextStyle: {
        color: colors.white,
        width: 24,
        height: 24,
        position: 'absolute',
        alignSelf: 'center',
        top: 12,
        right: 15,
        bottom: 0,
        left: 0,
    }
})

export default CustomHeader;