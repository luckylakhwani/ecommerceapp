export const Endpoints = {
    getProducts: 'products?limit={limit}'
}

const API_URL = 'https://fakestoreapi.com/'

export function getAPIURL(Endpoint: string) {
    return API_URL + Endpoint;
}