import {
    trackGlobalErrors,
    openInEditor,
    overlay,
    networking
} from 'reactotron-react-native';
import Reactotron from 'reactotron-react-native';

const _Reactotron = Reactotron.configure({
    name: 'GetInsured'
})
    .use(trackGlobalErrors())
    .use(openInEditor())
    .use(overlay())
    .use(networking());

if (__DEV__) {
    _Reactotron.connect();
    _Reactotron.clear();
}
