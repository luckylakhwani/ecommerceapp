import { axiosInstance } from "./AxioConfig";
export { axiosInstance };

import { getAPIURL, Endpoints } from "./Endpoints";
export { getAPIURL, Endpoints };